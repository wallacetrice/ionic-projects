import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.page.html',
  styleUrls: ['./aboutus.page.scss'],
})
export class AboutusPage implements OnInit {

  p1:any = "Paradise Group of Hotels is well-known in the hotel industry of Kashmir. Paradise Group of Hotels offers the best lush green valley view enhancing your comfort and luxury. Here you will experience a homely feeling in our services. Set in an ambiance of lush green of foliage and green lawns, the Paradise Group of Hotels are built in the most eco friendly manner. Having about 46+ luxurious rooms categorized in Super Deluxe, Deluxe Rooms and Suit Rooms.";
  p2:any = "The attention we pay is evident in every aspect of hospitality, from the courteous friendly staff, to the quality of the furnishings, decorations and the authenticity of the cuisine we prepare. The Nice Moments Restaurant and Paradise Restaurant are famous name in the industry providing multicuisine dining with delicious Kashmiri ,Indian & Chinese cuisine.";
  constructor() { }

  ngOnInit() {
  }

}
